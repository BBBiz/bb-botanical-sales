---
layout              : page-fullwidth
show_meta           : false
title               : "Shop"
header:
   image_fullwidth  : "header4.jpg"
breadcrumb          : true

permalink           : "/products/"
---

Natural Ingredients for Natural People

# Orange Pine Cleaner

![](/images/Idea2.png)



“I can be pretty lazy when it comes to cleaning, but I don’t want to use all the harsh chemicals that many cleaners have”. If this sound like you, you have come to the right place.

Take control of your health and your mess with a natural cleaner. The easiest way to ensure that you know what’s in your cleaning products is to make them yourself. What do you do if you don’t have the time to do kitchen chemistry? This all natural cleaner from BB Botanicals contains four simple ingredients Vinegar, Soap-nut Berry Extract, Pine Extract, and Orange Oil. It has the cleaning power of plant-based extracts and oils to help with grime, soap scum, sticky residue and everyday dirt. It also helps to deodorize surfaces without bleach or other harsh chemicals.


## Details
Cleans gunk, sticky residue, oils, messes and more. Just shake, spray and wipe it off! For tougher spots, let Orange Pine set for 5 minutes before wiping off.

## Caution
Not safe to use on some of porous and semi-porous surfaces. Cannot be used on granite or natural stone due to the acidic nature of vinegar. Will Stain white cloth. Keep out of reach of children, Maybe irritating to eyes or skin. Do not drink.
First aid: Contact: Rinse off with water if irritating. Eyes: Remove contacts and rinse
eye for 15 minutes, if still irritated see doctor. Swallowed: If small amount rinse mouth
out and drink a glass of water, if large amount it can be toxic call doctor.
Please recycle this bottle
Storage: Store away from flame and out of reach of children

## Ingredients
Vinegar, Soap-nut Berry Extract, Pine Extract, Orange Oil

