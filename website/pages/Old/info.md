---
layout: page
title: "About"

teaser: "permaculture farm located  in the northern 
                highlands of Vermont"
permalink: "/info/"
header:
    image_fullwidth: "header_drop.jpg"
---

     
        <H3>BB Botanical is a off-grid apothecary</H3>
            <p>We have 98 acre permaculture farm located  in the northern 
                highlands of Vermont, in the Green Mountains. We are a 
                company where tradition is coupled with scientific knowledge 
                to create quality products. Our goal is to create safe and 
                potent herbal products for your family’s health and vitality 
                while being stewards for the earth. On a summer day you might 
                find us in the food forest harvesting strong plants or in the 
                lab extracting there strength for you. We offer custom 
                products for a wide range of everyday remedies for the whole 
                family. Explore our diverse range of teas, tinctures, jams, 
                fruit leathers, cleansers, creams, and oils meticulously 
                crafted for you. All of our products and herbs are  
                pesticide-free and sourced from our land whenever possible. 
                All of our products are minimally processed and made in small 
                batches. We pride ourselves on providing exceptional products 
                that are specifically tailored to meet your specific needs. 
                We are your resource for a tasty fun way to take your herbs. 
                </p>
        <H3>Certified Organic</H3>
            <p>Why do we not want to be come certified organic. Because 
            certified organic means a lot less that it is lead to believe. 
            being certified organic means that a company payed a bunch of 
            money to a government organization that then makes sure that that 
            company checks some boxes. Most of it has little to do with best farming 
            practices and more with making money. So no we don't want to be certified 
            organic, we don't want to be apart of a broken system. We want to grow and 
            make the best quality plants, and products that we can. We do extensive research 
            and tests on how to be able to make the heights quietly that we can. We then also 
            pass any savings we have by not paying for fancy certifications on to our customers. 
            Our products still may not be the cheapest on the market because we put a 
            lot of energy and love into everything we make.</p>
        <H3>Permiculture</H3>
                <p>We grow our plants in alience with nature. We have 2 types of hugals which most of 
                our plants grow on. We have field hugals which are 2 feet tall hugals that mostly 
                our annuals and full sun plants grow on. Our perannuals grow on 5 foot hugals in 
                a food forest. The hugals allow us to not have to water as they naturally retain 
                the rain water for the plants druing times of drought. This means less water needs to 
                be pumped out of the ground and allowing nature to sustain life naturally.
                Intstead of using pesticites we use compainon planting as an example 
                plating radishs near plants that are having an issue with snails will draw the 
                snails on to the beets saving the other plants from the snals. We use almost no 
                outside fertlier. If we do have to use an outside fertlizer we get manure from a
                local outdoor grass fed cow farm. Primarly the fertlizer we make using worms and ducks.
                The primary diet of our ducks and worms is food we grow on our property. This ensures
                a healthy cyle of life pesticide free.</p>
        <H3>Sales</H3>
            <p>We don't do sales because sales is often a company that is instead of 
            selling the product at a 70% mark up is now selling it for 50% mark up. Which makes 
            it seem great, but when you are buying it not on sale you paying a more inflated 
            price than it needed to be. Here we are simply charging what it takes us to make and sell
            products in the best way we can. Our products our priced by the time, energy, and input 
            into each product. For example if you have ever looked at a menu and see that an egg 
            and sausage patty with toast cost more then an egg and sausage patty with toast that 
            is now in the form of a sandwich. That means they are not making a 1 to 1 for there 
            prices. Here we calculate exactly how much goes in to each product to determine the price. 
            So you know that every item you buy is made in the best possible way while giving 
            you the best value possible every time. </p>
    </div>
