---
title: "A website that doesn't exit"
layout: redirect
sitemap: false
permalink: /redirect-page/
redirect_to:  "http://phlow.github.io/feeling-responsive/info/"

header:
   image_fullwidth  : "header3.jpg"
breadcrumb          : true

---
This is just a page to demonstrate the `redirect`-layout, programmend by [Kanishk](http://codingtips.kanishkkunal.in/about/).