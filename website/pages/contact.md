---
layout              : page-fullwidth
show_meta           : false
title               : "Contact"
header:
   image_fullwidth  : "header4.jpg"
breadcrumb          : true
permalink           : "/contact/"
---
<form
  action="https://formspree.io/f/xwppaywd"
  method="POST"
>
  <label for="email" class="required" style="font-weight: bold;">
    Your email:
  </label>
    <input type="email" name="email" required>
  <label style="font-weight: bold;">
    Your Phone Number:
  </label>
    <input type="tel" id="phone" name="phone" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}">

  <label style="font-weight: bold;">How can we help?</label>

  <label style="font-weight: bold;">
    <textarea rows=10 name="message"></textarea>
  </label>
  <!-- your other form fields go here -->
  <button type="submit">Send</button>
</form>
