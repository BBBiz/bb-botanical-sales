---
layout: page
#
# Content
#
subheadline: "Summer"
title: "Our First Summer"
teaser: "Our first summer in Holland we were going back and forth to Burlington. We had no power no stove no fridge, and had a lot of fixing to do before we could actually move in.."
categories:
  - Set Up
tags:
  - Off Grid
  - Fixing
  - Set up
#
# Styling
#
header: no
image:
    title: mediaplayer_js-title.jpg
    thumb: mediaplayer_js-thumb.jpg
    homepage: mediaplayer_js-home.jpg
    caption: Photo by Corey Blaz
    caption_url: https://blaz.photography/
mediaplayer: true
---

Our first summer in Holland we were going back and forth to Burlington. We had no power no stove no fridge, and had a lot of fixing to do before we could actually move in.

For the first month our goal was to take down a quenst hut that had half fallen down in a wind storm during the one of the 5 years it was abandon. It was built as  basically a giant metal parasail behind the barn/garage, and luckily was only partly attached to it. The wind must have hit the back of the barn and went up into the parasail with enough force to pull half of the posts that were anchored into the ground up then twisted as it fell back to the ground. The bank would not insure the house with the hut still half in the air like it was and almost would not give us the loan. But lucky with us scrounging up an extra 7,000 and the current owner putting 5,000 into our down payment meant they would give us the loan but the hut had to be taken down in the first month. We had a couple deconstruction companies come out and look at but they had the same answer. 'We can take that down for you in less then 10 minuets but you can pay us enough to come all the way out here with the equipment to do it.' So finally we decided we had to do it ourselves. It was day 30 and the bank was expecting proof of it gone but the next morning. Ed got in the tractor and our wonderful nighbor came up with a chain and the two of them coordinated putting the chain on different spots until it finally fell the rest of the way down. then it was simply pushed to the side and photo proof was sent to the bank. 

We gave ourselves to August to make the place livable. We had to replace the roof over the porch that was completely rotten. Buy batteries, solar panels, inverters and all that what nots. The best part was how quiet breaks were there were no cars driving by or random people staring at you through the living room windows. It was so peaceful. The house even came a cuite red squrial that live in the porch. We named her squeaker because when we would hang out in the porch on the deck squeaker would come yell at us.
