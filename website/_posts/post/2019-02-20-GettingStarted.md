---
layout: page
#
# Content
#
subheadline: "Fall"
title: "Getting Started"
teaser: "In May of 2019 We purchases 90 acres in Holland VT. Soon to become known as the Wild Beaver Patch, Off-Grid Apothecary and home to BB Botanical..."
categories:
  - Set Up
tags:
  - Off Grid
  - Fixing
  - Set up
#
# Styling
#
header: no
image:
    title: mediaplayer_js-title.jpg
    thumb: mediaplayer_js-thumb.jpg
    homepage: mediaplayer_js-home.jpg
    caption: Photo by Corey Blaz
    caption_url: https://blaz.photography/
mediaplayer: true
---

In May of 2019 We purchases 90 acres in Holland VT. Soon to become known as the Wild Beaver Patch, Off-Grid Apothecary and home to BB Botanical.

We started looking for about 100 acres of just land in January of 2018. We looked at dozens of properties and drove all over the state before we found this place. We first saw the land when it was covered in snow and we had to snow shoe a mile to get up to where the house was. The first time we saw the property with out snow was on closing day. The day before there was huge rain storm that it washed parts of the road and a large tree had fallen blocking the way. Which meant our first day as owners we had to head back to town so we could chain saw our way to our new home.

We had bought a kubota with back hoe and plow attachment shortly after we were sure we had the house. It was delivered the first week after closing. The tractor was delivered to the end of our 1 mile drive where ed met the driver. It was his first time driving a tractor and was surprised on how many levers and buttons it had. Luckily our neighbor came over a little after the tractor had been delivered and showed ed the basics.
