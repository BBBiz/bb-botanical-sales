FROM ubuntu:22.04

ENV BUNDLE_PATH=/gembox

RUN apt update && \
    apt install -y \
            build-essential \
            gosu \
            ruby-full \
            webp \
            zlib1g-dev && \
    useradd --create-home \
            -d /home/ruby-user \
            --uid 1000 \
            --shell /bin/bash \
            ruby-user && \
    chown -R ruby-user:1000 /home/ruby-user && \
    chmod -R 755 /home/ruby-user &&\
    mkdir -p $BUNDLE_PATH && \
    chown -R ruby-user:1000 $BUNDLE_PATH

RUN gem install bundler \
                jekyll \
                jekyll-admin \
                jekyll-feed \
                just-the-docs \
                minima \
                webrick && \
    echo \
"#!/bin/bash\n\n \
set -e\n \
bundle install\n \
exec bundle exec jekyll build" >> /usr/local/bin/entrypoint && \
   chmod +x /usr/local/bin/entrypoint

WORKDIR /projects
USER ruby-user
EXPOSE 4000
